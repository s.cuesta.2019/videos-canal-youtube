import sys
import urllib.request
import xml.etree.ElementTree as ET

def generate_html(video_entries):
    html_content = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CursosWeb Videos</title>
    </head>
    <body>
        <h1>Videos del canal CursosWeb:</h1>
        <ul>
    """

    for entry in video_entries:
        title = entry['title']
        link = entry['link']
        html_content += f'<li><a href="{link}" target="_blank">{title}</a></li>\n'

    html_content += """
        </ul>
    </body>
    </html>
    """
    return html_content

def parse_rss(rss_url):
    try:
        with urllib.request.urlopen(rss_url) as response:
            xml_data = response.read()
    except urllib.error.URLError as e:
        print(f"Error al acceder a la URL: {rss_url}")
        return []

    video_entries = []
    root = ET.fromstring(xml_data)

    for entry in root.findall('.//{http://www.w3.org/2005/Atom}entry'):
        title = entry.find('{http://www.w3.org/2005/Atom}title').text.strip()
        link = entry.find('{http://www.w3.org/2005/Atom}link').attrib['href']
        video_entry = {'title': title, 'link': link}
        video_entries.append(video_entry)

    return video_entries

if __name__ == "__main__":
    rss_url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg"

    video_entries = parse_rss(rss_url)
    html_content = generate_html(video_entries)

    # Output HTML to stdout (use redirection to save to a file)
    print(html_content)
